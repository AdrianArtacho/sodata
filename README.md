# SoDATA #

This is the repository for the standalone application
**SoDATA**, which is an optional component of
the [**So**cial **D**istancing **A**plication Kit](https://bitbucket.org/AdrianArtacho/soda-node).
